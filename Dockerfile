 FROM mcr.microsoft.com/windows/servercore:1809
 
RUN powershell -NoProfile -ExecutionPolicy Bypass -Command \
    Invoke-WebRequest "https://aka.ms/vs/16/release/vs_buildtools.exe" \
    -OutFile "%TEMP%\vs_buildtools.exe" -UseBasicParsing

RUN "%TEMP%\vs_buildtools.exe"  --quiet --wait --norestart --noUpdateInstaller \
    --add Microsoft.VisualStudio.Component.VC.Tools.x86.x64 \
    --add Microsoft.VisualStudio.Component.VC.CMake.Project

SHELL ["cmd", "/wait", "/S", "/C"]
RUN setx /M PATH "%PATH%;C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Tools\MSVC\14.29.30133\bin\Hostx64\x64;C:\Program Files (x86)\Microsoft Visual tudio\2019\BuildTools\Common7\IDE\CommonExtensions\Microsoft\CMake\CMake\bin"
SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop';"]

