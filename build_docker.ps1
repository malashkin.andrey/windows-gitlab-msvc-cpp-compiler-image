$ErrorActionPreference = "Stop"

# Input parameters are IMAGE_NAME and DOCKERFILE_PATH and DOCKER_BUILD_ARGUMENTS. 
# If DOCKERFILE_PATH is not defined, current folder '.' will be used
# Default IMAGE_NAME is current project name

if ((Test-Path variable:IMAGE_NAME) -eq $False) 
{
    $IMAGE_NAME=${CI_PROJECT_NAME}
}
if ((Test-Path variable:DOCKERFILE_PATH) -eq $False) 
{
    $DOCKERFILE_PATH="."
}

.\docker_login.ps1

$CURRENT_IMAGE="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_SHA}"         # This variable is an output variable
$PREVIOUS_IMAGE="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:$(git rev-parse HEAD~1)" # This variable is a heper variable
$BRANCH_LATEST="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"    # This variable is a heper variable

docker pull $PREVIOUS_IMAGE
docker build --pull --cache-from $PREVIOUS_IMAGE $DOCKER_BUILD_ARGUMENTS -t $CURRENT_IMAGE ${DOCKERFILE_PATH}
.\push_image.ps1 $CURRENT_IMAGE
# update tag BRANCH_LATEST
docker tag $CURRENT_IMAGE $BRANCH_LATEST
.\push_image.ps1 $BRANCH_LATEST
