$ErrorActionPreference = "Stop"

$IMAGE_TO_PUSH=$args[0]
if ((Test-Path variable:IMAGE_TO_PUSH) -eq $False)
{
    echo "push_image requires image name as an argument"
    [Environment]::Exit(1)
}

echo "pushing image ${IMAGE_TO_PUSH}"

docker push $IMAGE_TO_PUSH
if($? -eq $False)
{
    echo "could not push image; try to login again"
    .\docker_login.ps1
    docker push $IMAGE_TO_PUSH
    if($? -eq $False)
    {
        [Environment]::Exit(1)
        echo "could not push image!"
    }
}
