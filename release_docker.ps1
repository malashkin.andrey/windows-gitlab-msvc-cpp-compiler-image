$ErrorActionPreference = "Stop"

Test-Path ENV:CI_COMMIT_REF_NAME

IF(${ENV:CI_COMMIT_REF_NAME}.equals("master"))
{

  Test-Path CURRENT_IMAGE
  Test-Path VERSION

  if ((Test-Path VERSION) -eq $False)
  {
    .\set_version.ps1
  }

  $RELEASE_IMAGE_NAME="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}/release:${VERSION}"
  docker tag "${CURRENT_IMAGE}" "${RELEASE_IMAGE_NAME}"
  .\push_image.ps1 "${RELEASE_IMAGE_NAME}"

  $LATEST_IMAGE_NAME="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}/release:latest"
  docker tag "${CURRENT_IMAGE}" "${LATEST_IMAGE_NAME}"
  .\push_image.ps1 "${LATEST_IMAGE_NAME}"
}
