$ErrorActionPreference = "Stop"

cd C:\sources
mkdir artifacts -ErrorAction SilentlyContinue

cmake . -DCMAKE_INSTALL_PREFIX=C:\sources\artifacts
cmake --build .

