# windows-gitlab-msvc-cpp-compiler-image

## Getting started

This project contains a windows build image sample, with this image you can produce windows binaries using GitLab CI.

You can use resulting docker image: windows-1809-gitlab-msvc-cpp-compiler-image
Or you can just copy-paste the project layout to your project and change build-on-windows job

## Contributing
Contributions are welcome

## License
Free for all

## Project status
In development

## For companies

