$ErrorActionPreference = "Stop"

git describe --always --tags --abbrev=9 | Tee-Object -Variable VERSION
echo "VERSION is set to $VERSION"
