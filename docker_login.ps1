$ErrorActionPreference = "Stop"

Test-Path Env:CI_REGISTRY_USER
Test-Path Env:CI_REGISTRY_PASSWORD
Test-Path Env:CI_REGISTRY

docker login -u ${Env:CI_REGISTRY_USER} -p ${Env:CI_REGISTRY_PASSWORD} ${Env:CI_REGISTRY}

